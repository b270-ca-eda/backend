
// 4. Create an index.js file inside of the activity folder.
// 5. Import the http module using the require directive.

let http = require("http");

// 6. Create a variable port and assign it with the value of 3000.

const port = 3000

// 7. Create a server using the createServer method that will listen in to the port provided above.
const server = http.createServer((request, response) => {

// 8. Console log in the terminal a message when the server is successfully running.
	
// 9. Create a condition that when the login route is accessed, it will print a message to the user that they are in the login page.


	if (request.url == '/login') {

		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Welcome to the login page.")

	
// 10. Access the login route to test if it’s working as intended.
// 11. Create a condition for any other routes that will return an error message.
	} else {
		response.writeHead(404, {'Content-Type': 'text/plain'});
		response.end("I'm sorry the page you are looking for cannot be found.")
	}

})

server.listen(port);
console.log("Server is successfully running at localhost:3000.");

// 12. Access any other route to test if it’s working as intended.
// 13. Create a git repository named S31.
// 14. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// 15. Add the link in Boodle.