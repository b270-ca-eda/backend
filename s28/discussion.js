//CRUD Operations
/*
	- CRUD operations are the heart of any backend applications
	- Mastering the CRUD operations of any language makes us a valuable developer and makes the work easier for us to deal with huge amounts of information
*/

 // [SECTION] Inserting documents (CREATE)
 /*
	Syntax:
		db.collectionName.insertOne({object})

		Insert Many:
		db.collectionName.insertMany([ {objectA}, {objectB} ])
 */

db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "09123456789",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "Javascript", "Python"],
	department: "none"
})

//Insert Many
db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "0987654321",
			email: "stephenhawking@gmail.com"
		},
		courses: ["Python", "React", "1php"],
		department: "none"
	},
	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "0987654321",
			email: "neilarmnstrong@gmail.com"
		},
		courses: ["React", "Laravel", "Sass"],
		department: "none"
	}
])

// [SECTION] Finding documents (READ/RETRIEVING DOCUMENT)
/*
	- Syntax:
		db.collectionName.find();
		db.collectionName.find({ filed: value});
*/

// Leaving the search criteria empty will retrieve ALL of the docuemtns

db.users.find();

// Finding a single document
db.users.find({ firstName: "Stephen"});
db.users.find({ age: 82});

//Finding documents with multiple parameters
/*
	- Syntax:
		db.collectionName.find({ fieldA: valueA, fieldB: valueB });
*/
db.users.find({ lastName: "Armstrong", age: 82});
db.users.find({ firstName: "Jane", age: 21});


// [SECTION] Updating documents (UPDATE)

db.users.insertOne({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "00000000",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
})


// Updating a single document
/*
	Syntax:
		db.collectionName.updateOne({criteria}, {$set: {field: value}});
*/

db.users.updateOne(
	{firstName: "Test"},
	{
		$set: {
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "12345678",
				email: "bill@gmail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations",
			status: "active"
		}
	}

)
db.users.find({ firstName: "Bill"});

//Updating multiple documents
/*
	- Syntaxt:
		db.collectionName.updateMany({criteris, {$set: {field: value}}})
*/
db.users.updateMany(
	{"department":"None"},
	{
		$set: {department: "HR"}
	}	
)

// Replace One
/*
	- Can be used if replacing the while document is necessary
*/
db.users.replaceOne(
	{ firstName: "Bill"},
	{
		firstName: "Bill",
		lastName: "Gates",
		age: 65,
		contact: {
			phone: "12345678",
			email: "bill@gmail.com"
		},
		courses: ["PHP", "Laravel", "HTML"],
		department: "Operations"
	}
)

// Deleting documents (DELETE) 
db.users.insertOne({
	firstName: "Test"
})

// Deleting a single document
/*
	- Syntax:
	db.collecctionName.deleteOne({Criteria})
*/

db.users.deleteOne({firstName: "Test"});

// Delete Many
/*
	- Be careful when using the deleteMany. If no search criteria is provided, it will delete all the documents in a database
*/

db.users.insertOne({firstName: "Bill",
	lastName: "Gates",
	age: 0,
	contact: {
		phone: "00000000",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"})


db.users.deleteMany({firstName:"Bill"})

// [SECTION] Advanced Queries

// Query on embedded document
db.users.find({
	contact:  {
		phone: "0987654321",
		email: "stephenhawking@gmail.com"
	}
})

// Querying an array
db.users.find({
	course: ["CSS," "Javascript", "Python"]
})

// Querying an array without regard to order
db.users.find({
	courses: {$all: ["React", "Python"]}
})

//Querying an embedded array

db.users.insertOne({
	namearr: [
		{
			namea: "Juan"
		},
		{
			nameb: "tamad"
		}
	]
})

db.users.find({
	namearr:
		{
			namea: "Juan"
		}
})