// Controllers contain the functions and business logic of our Express application
// All the operation that it can do will be placed in this file

const Task = require("../models/task");

module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result
	})
}

module.exports.createTask = (requestBody) => {

	// Creates a task object based on the model Task
	let newTask = new Task({

		//Sets the name property with the value received from the client/postman
		name: requestBody.name
	})

	return newTask.save().then((task, error) => {

		if (error){
			console.log(error);
			return false
		} else {
			return task;
		}
	})
}

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err) {
			console.log(err);
			return false
		} else {
			return removedTask;
		}
	})
}

module.exports.updateTask = (taskId, newContent) => {

	return Task.findById(taskId).then((result, error) => {

		if(error) {
			console.log(error);
			return false;
		}

		//Result of the "findById" will be stored in the "result" parameter
		// Its "property" will be reassigned to teh value of the "name" received from the request 
		result.name = newContent.name;

		return result.save().then((updatedTask, saveErr) => {
			if(saveErr) {
				console.log(saveErr);
				return false;
			} else {
				return updatedTask;
			}
		})
	})
}

//ACTIVITY

/*

S36 Activity:
1. Create a route for getting a specific task.
2. Create a controller function for retrieving a specific task.
3. Return the result back to the client/Postman.
4. Process a GET request at the "/tasks/:id" route using postman to get a specific task.
5. Create a route for changing the status of a task to "complete".
6. Create a controller function for changing the status of a task to "complete".
7. Return the result back to the client/Postman.
8. Process a PUT request at the "/tasks/:id/complete" route using postman to update a task.
9. Create a git repository named S31.
10. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
11. Add the link in Boodle.
*/

module.exports.getSpecificTask = (taskId) => {
	return Task.findById(taskId).then(result => {
		return result
	})
}

module.exports.changeStatus = (taskId, newStatus) => {
	return Task.findById(taskId).then((result, error) => {
		if(error) {
			console.log(error);
			return false;
		}

		result.status = newStatus.status;

		return result.save().then((updatedStatus, saveErr) => {
			if(saveErr) {
				console.log(saveErr);
				return false;
			} else {
				return updatedStatus;
			}
		})
	})
}
