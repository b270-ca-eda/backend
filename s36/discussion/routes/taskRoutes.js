// Contains all hte endpoints for our application

const express = require ("express");

const taskController = require("../controllers/taskController");

// Creates a Router instance that functons as a middleware and routing system
// Allows access to HTTP method middleware that make it easier to create routes for our application
const router = express.Router();

// [SECTION] Routes

//Route to get all the tasks
router.get("/", (req, res) => {

	// Invokes the "getAllTasks" function from the "taskController.js" file 

	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
})

// Route to create a new task
router.post("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})

//Route to delete a task

router.delete('/:id', (req, res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController))
})


//Route to update a Task
router.put("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})

//ACTIVITY

/*

S36 Activity:
1. Create a route for getting a specific task.
2. Create a controller function for retrieving a specific task.
3. Return the result back to the client/Postman.
4. Process a GET request at the "/tasks/:id" route using postman to get a specific task.
5. Create a route for changing the status of a task to "complete".
6. Create a controller function for changing the status of a task to "complete".
7. Return the result back to the client/Postman.
8. Process a PUT request at the "/tasks/:id/complete" route using postman to update a task.
9. Create a git repository named S31.
10. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
11. Add the link in Boodle.
*/

router.get("/:id", (req, res) => {
	taskController.getSpecificTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

router.put("/:id/complete", (req, res) => {
	taskController.changeStatus(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})
// Exports the router object to be used in the "index.js"
module.exports = router;