const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

// Check if the email already exists
/*
	Steps: 
	1. Use mongoose "find" method to find duplicate emails
	2. Use the "then" method to send a response back to the frontend appliction based on the result of the "find" method
*/

module.exports.checkEmailExists = (req, res) => {
	return User.find({email: req.body.email}).then(result => {
		
		// the "find" method return a record if a match is found
		if(result.length >0) {
			return res.send(true);
		
		// No duplicate email found
		// The user is not yet registered in the database
		} else {
			return res.send (false);
		}
	})
	.catch(error => res.send(error))
}


// User registration
/*
	Steps:
	1. Create a new User object using the mongoose model and the information from the request body
	2. Make sure that the password is encrypted
	3. Save the new User to the database
*/

module.exports.registerUser = (req, res) => {

	// Creates a variable "newUser" and instantiates a new "User" object using the mongoose model
	// Uses the information from the request body to provide all the necessary information
	 let newUser = new User({
		firtsName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,

		// 10 is the value provided as the number of "salt" rounds that the bcrypt algorythm will run in order to encrypt the password
		//Syntax: bcrypt.hashSync(dataToBeEncrypted, saltRounds)
		password: bcrypt.hashSync(req.body.password, 10),
		mobileNo: req.body.mobileNo
	})

	 //Saved the created object to our database
	 return newUser.save().then(user => {
	 	console.log(user);
	 	res.send(true)
	 })
	 .catch(error => {
	 	console.log(error);
	 	res.send(false);
	 })
}


// User authentication
/*
	Steps:
	1. Check the database if the user email exists
	2. Compare the password provided in the login form with the password stored in the database
	3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/

module.exports.loginUser = (req, res) => {
	
	return User.findOne({email: req.body.email}).then(result => {

		// User does not exist
		if (result == null) {

			return res.send({message: "No user found"});

		// User exists
		} else {

			// Creates the variable "isPasswordCorrect" to return the result of comparing the login form password and the database password
			// The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);


			if (isPasswordCorrect) {

				//GEnerate and access token by invoking the "createAccessToken" in the auth.js file 
				return res.send({accessToken: auth.createAccessToken(result)});

			//Passwords do not match
			} else {
				return res.send(false)
			}
		}
	})
}

//ACTIVITY
/*
S38 Activity
1. Create a "/details" route that will accept the user’s Id to retrieve the details of a user.
2. Create a getProfile controller method for retrieving the details of the user:
    - Find the document in the database using the user's ID
    - Reassign the password of the returned document to an empty string
    - Return the result back to the postman
3. Process a GET request at the /details route using postman to retrieve the details of the user.
4. Updated your s37-41 remote link, push to git with the commit message of Add s38 activity code
5. Add the link in Boodle.
*/

// My solution which is acceptable also
/*module.exports.getProfile = (req, res) => {

	return User.findById(req.body.id).then(result => {

		if (result == null) {
			return res.send({message: "No user found"});

		} else {
			let profile = result.toObject();
			result.password = "";
			return res.send(profile);
	}
})
}*/

//Other's solution which is also correct
/*
module.exports.getProfile = (req, res) => {

    return User.findById(req.body.id).then(result => {
        // User does not exists
        if(result == null){
            return res.send({message: "No user found"});

        // User exists
        } else {
            let userDetails = result.toObject();
            userDetails.password = "";
            return res.send(userDetails);
        }
    })
    .catch(error => res.send(error));
}*/

module.exports.getProfile = (req, res) => {
	
	// Uses the "decode" method defined in the "auth.js" file to retrieve the user information from the token passing the "token" from the request header as an argument
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	return User.findById(userData.id).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		result.password = "";

		// Returns the user information with the password as an empty string
		return res.send(result);
	});

};

// Enroll user to a course
/*
	Steps:
	1. Find the document in the database using the user's ID
	2. Add the course ID to the user's enrollment array
	3. Update the document in the MongoDB Database

	Workflow:
	1. User logs in, server will respond with JWT on successful authentication
	2. A POST request with the JWT in its header and the course ID in its request body will be sent to the /users/enroll endpoint
	3. Server validates JWT
		- If valid, user will be enrolled in the course
		- If invalid, deny request
*/

module.exports.enroll = async (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(!userData.isAdmin) {
		// To retrieve the course name
		let courseName = await Course.findById(req.body.courseId)
		.then(result => result.name);


		let data = {

			// User ID and email will be retrieved from the payload/token
			userId: userData.id,
			email: userData.email,
			// Course ID will be retrieved from the request body
			courseId: req.body.courseId,
			courseName: courseName
		}
		console.log(data);


		// Add the course ID in the enrollments array of the user
		// Creates an "isUserUpdated" variable and returns true upon successful update otherwise false
		// Using the "await" keyword will allow the enroll method to complete updating the user before returning a response back to the frontend

		let isUserUpdated = await User.findById(data.userId).then(user => {

			// Adds the courseId in the user's enrollments array
			user.enrollments.push({
				courseId: data.courseId,
				
			});

			// Saves the updated user information in the database
			return user.save().then(result => {
				console.log(result);
				return true;
			})
			.catch(error => {
				console.log(error);
				return false;
			});
		});
		console.log(isUserUpdated);


		// Add the user ID in the enrollees array of the course
		// Using the "await" keyword will allow the enroll method to complete updating the course before returning a response back to the frontend

		let isCourseUpdated = await Course.findById(data.courseId)
		.then(course => {

			// Adds the userId in the course's enrollees array
			course.enrollees.push({
				userId: data.userId
			})
		
			// Minus the slots available by 1
			course.slots -= 1;

			// Saves the updated course information in the database
			return course.save().then(result => {
				console.log(result);
				return true;
			})
			.catch(error => {
				console.log(error);
				return false;
			});
		});
		console.log(isCourseUpdated);


		// Condition that will check if the user and course documents have been updated

		// User enrollment successful
		if(isUserUpdated && isCourseUpdated) {
			return res.send(true);

		// User enrollment failure
		} else {
			return res.send(false);
		}
	} else {
		return res.send(false);
	}

	

}