const Course = require("../models/Course");
const auth = require("../auth.js");

// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new User to the database
*/

module.exports.addCourse = (req, res) => {

	// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
	// Uses the information from the request body to provide all the necessary information

// My solutoion to s39 activity
	/*let newCourse = new Course({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
		slots: req.body.slots
	});


	const user = auth.decode(req.headers.authorization);
	console.log(user);
	

	if (user.isAdmin === true){
	return newCourse.save().then(course => {
		console.log(course);
		res.send(true);
	})
	} else {
		res.send(false);
	}
}*/

/*
S39 Activity:
1. Refactor the course route to implement user authentication for the admin when creating a course.
2. Refactor the addCourse controller method to implement admin authentication for creating a course.
3. Push to git with the commit message of Add activity code - S39.
4. Add the link in Boodle.
*/

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	if(userData.isAdmin) {
		// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newCourse = new Course({
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			slots: req.body.slots
		});

		// Saves the created object to our database
		return newCourse.save()
		// Course creation is successful
		.then(course => {
			console.log(course);
			res.send(true);
		})
		// Course creation failed
		.catch(error => {
			console.log(error);
			res.send(false);
		})
	} else {
		return res.send(false);
	}
	
}

// Retrieve all courses
/*
	Step:
	1. Retrieve all the courses from the database
*/
module.exports.getAllCourses = (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin) {
	return Course.find({}).then(result => res.send(result))
	} else {
		return res.send(false);
	}
}


// Retrieve all ACTIVE courses
/*
	Step:
	1. Retrieve all the courses from the database with property of "isActive: true"
*/
module.exports.getAllActive = (req, res) => {

	return Course.find({isActive: true}).then(result => res.send(result));
}


// Retrieving a specific course
/*
	Steps:
	1. Retrieve the course that matches the course ID provided in the URL
*/
module.exports.getCourse = (req, res) => {

	console.log(req.params.courseId)

	return Course.findById(req.params.courseId)
	.then(result => {
		console.log(result);
		return res.send(result)
	})
	.catch(error => {
		console.log(error);
		return res.send(error);
	})
}


// Update a course
/*
	Steps:
	1. Create a variable "updatedCourse" which will contain the information retrieved from the request body
	2. Find and update the course using the course ID retrieved from the request params property and the variable "updatedCourse" containing the information from the request body
*/
// Information to update a course will be coming from both the URL parameters and the request body

module.exports.updateCourse = (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin) {
		let updateCourse = {
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			slots: req.body.slots
		}

		//Syntax: findByIdAndUpdate(documentId, updatesToBeApplied)
		// {new:true} = returns teh updated document
		return Course.findByIdAndUpdate(req.params.courseId, updateCourse, {new:true}).then(result => {

			console.log(result);
			res.send (result);
		})
	.catch(error => {
		console.log(error);
		res.send(false);
	})
	} else {
		return res.send(false);
	}
}

/*
S40 Activity:
    1. Create a route for archiving a course. The route must use JWT authentication and obtain the course ID from the url.
    2. Create a controller method for archiving a course obtaining the course ID from the request params and the course information from the request body.
    3. Process a PATCH request at the /courseId/archive route using postman to archive a course
    4. Push to git with the commit message of Add activity code - S40.
    5. Add the link in Boodle.
*/

// Archive a course / Unarchiving
// In managing databases, it's common practice to soft delete our records and what we would implement in the "delete" operation of our application
// The "soft delete" happens here by simply updating the course "isActive" status into "false" which will no longer be displayed in the frontend application whenever all active courses are retrieved
// This allows us access to these records for future use and hides them away from users in our frontend application
// There are instances where hard deleting records is required to maintain the records and clean our databases
// The use of "hard delete" refers to removing records from our database permanently

module.exports.archiveCourse = (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin) {
		let archiveCourse = {
			isActive: req.body.isActive
		}

		return Course.findByIdAndUpdate(req.params.courseId, archiveCourse, {new:true}).then(result => {
			console.log(result);
			res.send(true);
		})
		.catch(error =>{
			console.log(error);
			res.send(false);
		})
 	} else {
 		return res.send(false);
 	}
 }





