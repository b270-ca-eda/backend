const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require("../auth");

// Route for creating a course
router.post("/", auth.verify, courseController.addCourse);


/*
S39 Activity:
1. Refactor the course route to implement user authentication for the admin when creating a course.
2. Refactor the addCourse controller method to implement admin authentication for creating a course.
3. Push to git with the commit message of Add activity code - S39.
4. Add the link in Boodle.
*/

// Route for retrieving all courses
router.get("/all", auth.verify, courseController.getAllCourses);

// Route for retrieving all ACTIVE courses
router.get("/", courseController.getAllActive);

// Route for retrieving a specific course
// Creating a route using the "/:parameterName" creates a dynamic route, meaning the url is not static and changes depending on the information provided in the url
router.get("/:courseId", courseController.getCourse);


// Route for updating a course
router.put("/:courseId", auth.verify, courseController.updateCourse);


// Route to archiving a course
// A "PATCH" request is used instead of "DELETE" request because of our approach in archiving and hiding the courses from our users by "soft deleting" records instead of "hard deleting" records which removes them permanently from our databases

// Route for archiving a course
router.patch("/:courseId/archive", auth.verify, courseController.archiveCourse);





module.exports = router;
