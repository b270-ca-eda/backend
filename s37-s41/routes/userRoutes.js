const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

// Route for chcking if the user's email alredy exists in the database
//Invokes the checkEmailExists function from the controller to communicate with our database
router.post("/checkEmail", userController.checkEmailExists);

//Route for user registration
router.post("/register", userController.registerUser);

//Route for user authentication
router.post("/login", userController.loginUser);

//Activity
/*
	S38 Activity
1. Create a "/details" route that will accept the user’s Id to retrieve the details of a user.
2. Create a getProfile controller method for retrieving the details of the user:
    - Find the document in the database using the user's ID
    - Reassign the password of the returned document to an empty string
    - Return the result back to the postman
3. Process a GET request at the /details route using postman to retrieve the details of the user.
4. Updated your s37-41 remote link, push to git with the commit message of Add s38 activity code
5. Add the link in Boodle.
*/

//Route to retrieve user details using ID
// Modified version - to give access to authorized users only (s39 discussion)
// The "auth.verify" acts as a middleware to ensure that the user is logged in before they can get the user details
router.get("/details", auth.verify, userController.getProfile);
module.exports = router;

// Route for enrolling a user to a course
router.post("/enroll", auth.verify, userController.enroll);
