//Dependencies
const express = require("express");
const mongoose = require("mongoose");
//allows our backend application to be available to our frontend application
const cors = require("cors");
const app = express();
//Allows access to routes defined within the application
const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");

//Database connection
mongoose.connect("mongodb+srv://admin:admin123@zuitt.1bvjamd.mongodb.net/b270_booking?retryWrites=true&w=majority",	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}	
);


// Set notification for database connection success or failure 
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"));
db.once("open", () => console.log("We're connected to the cloud database"))


//Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));
//Defines the "/users" to be included for all user routes
app.use("/users", userRoutes);
//Defines the "/course" to be included for all course routes
app.use("/courses", courseRoutes);


// Will use the defined port number for the application whenever an environment variable is available OR will use port 4000 if none is defined
// This syntax will allow flexibility when using the application locally or as a hosted application
app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`);
})
