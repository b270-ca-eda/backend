// console.log("Happy Wednesday!");

// Array Methods
// Javascript has built-in functions and methods for arrays. This allows us to manipulate and access array items

// [SECTION] Mutator Methods
/*
    - These are methods that "mutate" or change an array after they are created.
    - These methods manipulate the original array performing various tasks such as adding and removing elements
*/

let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];

// push()
/*
    - Adds an element at the end of an array AND returns the new array's length
    - Syntax:
        arrayName.push(elementToBeAdded);
*/

console.log("Current fruits array:");
console.log(fruits);

let fruitsLength = fruits.push("Mango");
console.log(fruitsLength); //5 - array's length
console.log("Mutated array from push method:");
console.log(fruits);

// Adding multiple elements to an array
fruits.push("Avocado", "Guava");
console.log("Mutated array from push method:");
console.log(fruits);

// pop()
/*
    - Removes the last element in an array AND returns the removed element
    - Syntax:
        arrayName.pop();
*/
let removedFruit = fruits.pop();
console.log(removedFruit); //Guava

console.log("Mutated array from pop method: ");
console.log(fruits);

// unshift()
/*
    - Adds one or more elements at the beginning of an array AND returns the new array
    - Syntax:
    arrayName.unshift(elementA, elementB);
*/
fruits.unshift("Lime", "Banana");
console.log("Mutated array from unshift method: ");
console.log(fruits);

// shift()
/*
    - Removes an element at the beginning of an array and returns the removed element
    - Syntax:
        arrayName.shift();
*/
let anotherFruit = fruits.shift();
console.log(anotherFruit); //Lime
console.log("Mutated array from shift method: ");
console.log(fruits);

// splice()
/*
    - Simultaneously removes elements from a specified index number and adds elements.
    - Returns the removed element/s
    - Syntax:
        arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
*/

let fruitSplice = fruits.splice(1, 2, "Lime", "Cherry");
console.log(fruitSplice); //Apple and Orange - removed elements
console.log("Mutated array from splice method: ");
console.log(fruits);

// sort()
/*
	-Rearranges the array elements in alphaneumeric order
	Syntax:
		arrayName.sort();
*/
fruits.sort();
console.log("Mutated array from sort method");
console.log(fruits);

// reverse()
/*
	- Reverse the order of the array elements
	Syntax:
		arrayName.reverse();
*/
fruits.sort();
console.log("Mutated array from reverse method");
console.log(fruits);

// Non-Mutator Methods
/*
	- Non-mutator methods are methods that do not modify or change an array after they're creates
	- These methods do not manipulate the original array performing various tasks such as returning elemetns from an array combining arrays and printing outputs
*/

let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];
console.log(countries);

// indexOf()
/*
	- Returns the index number of the first matching element found in an array
	- The search process will start from the first elemetn proceeding to the last element
	- Syntax:
		arrayName.indexOf(searchValue);
*/

/*let firstIndex = countries.indexOf("PH");
console.log("Result of indexOf method: " + firstIndex); //1*/

/*The search process will be done from the last element proceeding to the last element*/

let firstIndex = countries.indexOf("PH", 2);
console.log("Result of indexOf method: " + firstIndex); //5

let invalidCountry = countries.indexOf("BR");
console.log("Result of indexOf method: " + invalidCountry); //-1

// lastIndexOf()
/*
	- Returns the index number of the last matching element found in the array
	- The search process will be done from the last element proceeding to the frist element
	-Syntax:
		arrayName.indexOf(searchValue);*/
let lastIndex = countries.lastIndexOf("PH", 4);
console.log("Result of lastIndex method: " + lastIndex); //5

// slice()
/*
	- Portions/slices elements from an array and returns a new/sliced array.
	- Syntax:
		arrayName.slice(startingIndex);
*/


// Slicing off elements from a specified index to the last element
let slicedArrayA = countries.slice(2);
console.log("Result of slice method: ");
console.log(slicedArrayA); //[Can, SG, Th, PH, FR, DE]

// Slicing off elements starting from a specified index to another index
let slicedArrayB = countries.slice(2, 5); //index 2, 3, 4 (before 5)
console.log("Result of slice method: ");
console.log(slicedArrayB); //[CAN SG TH]

// Slicing off elements starting from the last element of an array
let slicedArrayC = countries.slice(-3);
console.log("Result of slice method: ");
console.log(slicedArrayC); //[PH, FR, DE]

// toString()
/*
	- Returns an array as a string seprated by commas
*/
let stringArray = countries.toString();
console.log("Result from toString method: ");
console.log(stringArray); //US,PH,CAN,SG,TH,PH,FR,DE

// concat()
/*
	- Combines arrays and returns the combined result/array
	-Syntax:
		arrayName.concat(arrayB)
*/
let taskArrayA = ["drink HTML","eat javascript"];
let taskArrayB = ["inhale css","breathe bootstrap"];
let taskArrayC = ["get git","be node"];

let tasks = taskArrayA.concat(taskArrayB);
console.log("Result from concat method: ");
console.log(tasks); //'drink HTML', 'eat JS', 'inhale css', 'breathe BS']

// Combining multiple Arrays
console.log("Result from concat method: ");
let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
console.log(allTasks); //['drink HTML', 'eat JS', 'inhale css', 'breathe BS', 'get git', 'be node']

// Combing Arrays from elements
let combinedTasks = taskArrayA.concat("smell express", "throw react");
console.log("Result from concat method: ");
console.log(combinedTasks);

//join()
/*
	- Returns an array as a string separated by a specified separator
	- Syntax:
		arrayName.join("separator")

*/
let users = ["John","Jane","Joe","Josh"];

console.log(users.join());
console.log(users.join(" "));
console.log(users.join(" - "));

//[SECTION] Iteration Methods

//forEach()
/*
	- Similar to a for loop that iterates on each array element
	- For each item in the array, the anonymous function passed in the forEach() will be run
	- The anonymous function is able to receive the current item being iterated or looped over by assigning a parameter
	- Does not return anything
	- Syntax:
		arrayName.forEach(function(indivElement) {
			statement
		})
*/

let filteredTasks = [];

allTasks.forEach(function(task) {

	if(task.length > 10) {

		filteredTasks.push(task);
	}
})

console.log("Result of filtered tasks: ");
console.log(filteredTasks);

//map()
/*
	- Iterates on each element and returns a new array.
	- Syntax:
		arrayName.map(function(indivElement){
			statement
		})
*/
let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function(number) {
	return number * number;
})

console.log("Original Array: ");
console.log(numbers); //[1, 2, 3, 4, 5]
console.log("Result of map method: ");
console.log(numberMap); //[1, 4, 9, 16, 25]

// map() vs forEach()

let numberForEach = numbers.forEach(function(number) {
	return number * number;
})
console.log(numberForEach); // undefined - becuase forEach() does not return anything

// every()
/*
	- Checks if all elements in an array meet the given condition
	- Returns a true value if all elements meet the condition and falce if otherwise
	- All has to be true
	- Returns a boolean
*/

let allValid = numbers.every(function(number){		
	return (number < 3);
})
console.log("Result of every method: ");
console.log(allValid);

// some()
/*
	- Checks if at least one elemetn in the array meets the given condition
	- Returns a true value if some elements meet the condition and false if none
*/
let someValid = numbers.some(function(number){
	return (number<2);
});
console.log("Result of some method: ");
console.log(someValid);

// filter()
/*
	- Returns a new array that contains the elements which meet the given condition
	- Returns an empty array if no elements were found
*/

let filterValid = numbers.filter(function(number) {
	return (number < 3);
})
console.log("Result of filter method: ");
console.log(filterValid);

//includes()

let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

let productFound1 = products.includes("Mouse");
console.log(productFound1); //true

let productFound2 = products.includes("Headset");
console.log(productFound2); //false

// reduce()
/*
	-Evaluates elements from left to right and returns/reduces the array into a single value
*/
let iteration = 0;

let reducedArray = numbers.reduce(function(x, y) {
	console.warn("Current iteration: " + ++iteration);
	console.log("Accumulator: " + x);
	console.log("currentValue: " + y);

	return x + y;
})
console.log("Result of reduce method: " + reducedArray);

let list = ["Hello", "Again", "World"];

// 1. x = Hello, y = Again => Hello Again
// 2. x = Hello Again + y = World => Hello Again world - reduced value

let reducedString = list.reduce(function(x, y){
	return x + " " + y;
})

console.log("Result of reduce method: " + reducedString);