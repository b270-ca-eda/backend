// alert("Hello, Batch270");

// This is a statement.

console.log("Hello again!");
console. log("Hi, Batch 270!")

/*
	- There are 2 types of comments:
		1. Single-line denoted by 2 slashes. (CTRL+/)
		2. Multi-line denoted by a slash and asterisk (CTRL+SHIFT+/)
*/

// (SECTION) Variables
	/*
		- It is used to contain data.
	*/

// Declaring Variables
// Syntax: let/const variableName;
/*
	- Trying to print out a value of a variable that has not been declared will return and error of undefined
	- Variables must be declared frst with value before they can be used
*/
	let myVariable;
	console.log(myVariable); 

	let greeting = "Hello";
	console.log(greeting);

// Declaring and initializing variables
// Initializing variables - the instance when a variable is given its initial value
// Syntax: let/const variableName=value;
// Let vkeyword is used if we want to reassign values to out variable
let productName = 'desktop computer';
productName = "Laptop"
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;
// interest = 2.5;
console.log(interest);

let friend = "Kate";
console.log(friend);

// we cannot re-declare variables within its scope
let friend1 = "Jay";
console.log(friend1)


// (SECTION) Data Types

// String - series of characters that creates a word, a phrase, a sentence or anything related to creating a text
// String can be written using either single or double quote.
let country = "Philippines";
let province = "Batangas";
console.log(country);
console.log(province); 

// Concatinating strings
// Mutiple strong values can be combined to create a single string using "+" symbol
let fullAddress = province + ", " + country;
console.log(fullAddress);
console.log("I live in the "+ country);

let mailAddress = "Metro Manila\n\nPhilippines";
console.log(mailAddress);

let message = 'John\'s employees went home early';
console.log(message);

// Numbers
	// Integers/Whole Numbers
	let headCount = 26;
	console.log(headCount);

	// DEcimal Numbers
	let grade = 98.7;
	console.log(grade);

	// Exponential Notation
	let planetDistance = 2e10;
	console.log(planetDistance);

// Boolean - value is either true or false

	let isMarried = false;
	let inGoodConduct = true;
	console.log("isMaried; " + isMarried);
	console.log("inGoodConduct: " + inGoodConduct)

// Arrays - are special kind of data type that's used to store multiple vales
// Arrays can sotre different data types but is normally used to store similar data types
// Syntax: let/const arrayName = [elementA, elementB, ...]

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

let details = ["John", "Smith", 32, true];
console.log(details);

// Objects
/*
	Syntax:
	let/const objectName = {
		propertyA: valueA,
		propertyB: valueB
	}
*/

let person = {
	fullName: "Juan Dela Cruz",
	age: 35,
	isMarried: false,
	contact: ["0912345678", "0987654321"],
	address: {
		housenumber: "345",
		city: "Manila"
	}
};
console.log(person);

// Null

let spouse = null;
console.log(spouse);