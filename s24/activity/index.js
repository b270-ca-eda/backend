	console.log("Hello");
/*S24 Activity:
1. In the S24 folder, create an activity folder and an index.html and index.js file inside of it.
2. Link the script.js file to the index.html file.
3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)*/

	const getCube = 2 ** 3;


/*
4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…*/

	console.log(`The cube of 2 is ${getCube}`);

/*
5. Create a variable address with a value of an array containing details of an address.*/

	const address = ["123", "Watermelon Street", "Brgy Bawa Bawa", "Dikalakihan City", "Cebu"]

/*
6. Destructure the array and print out a message with the full address using Template Literals.*/

	const [houseNo, stName, brgy, city, province] = address
	console.log(`You'll find me in ${houseNo} ${stName} ${brgy} ${city} ${province}, but this place is not in the map. Where am I?`)

/*
7. Create a variable animal with a value of an object data type with different animal details as it’s properties.*/

	let doggy = {
		name: "Bibi",
		age: "1.25 year old",
		breed: "princess shih tzu"
	}

/*
8. Destructure the object and print out a message with the details of the animal using Template Literals.*/

	let { name, age, breed } = doggy;
	console.log(`My dog's name, which is a ${breed}, is ${name}. She is ${age} already.`);

/*
9. Create an array of numbers.*/

	let numbers = [1, 2, 3, 4, 5];

/*

10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.*/


	numbers.forEach((numbers) => {
		console.log(numbers);
	})



/*
11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.*/


let reduceNumber = numbers.reduce((x, y) => {
/*	return a+b+c+d+e;*/
	return x+y;

})
console.log(reduceNumber);

/*
12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.*/

	class Dog{
		constructor(name, age, breed){
			this.name = name;
			this.age = age;
			this.breed = breed;
		}
	}

/*
13. Create/instantiate a new object from the class Dog and console log the object.*/

	const doggo = new Dog("Bibi", 1.25, "Shih Tzu");
	console.log(doggo);

/*
14. Create a git repository named S24.
15. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
16. Add the link in Boodle.*/