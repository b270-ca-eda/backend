// console.log("Hello, Batch 270!");

// [SECTION] ES6 Updates
// ES6 is also known as ECMAScript 2015 is an update to the previous versions of ECMAScript
// ECMA - European Computer Manufacturers Association
// ECMAScript is the standard that is used to create implementation of the language, one which is JavaScript

// New Features of JavaScript

// [SECTION] Exponent Operator
// Using the exponent operator
const anotherNum = 8 ** 2;
console.log(anotherNum);


// Using Math object methods
const num = Math.pow(8, 2);
console.log(num);//64


// [SECTION] Template Literals

// Pre-Template Literals
// Uses single/double quotes(""/'')
let name = "Daisy";

let message = "Hello " + name + "! \n Welcome to programming!";
console.log(message);

// String Using Template Literals
// Uses backticks (``) instead of ('') or ("")
message = `Hello ${name}! Welcome to programming`;
console.log(`Message with template literals: ${message}`);

// Multi-line Using Template Literals
const anotherMessage = `${name} attended a math competition.
He won it by solving the problem 8 ** 2 with the solution of ${anotherNum}.`
console.log(anotherMessage);

/*
	- Template literals allow us to write strings with embedded JavaScript expressions
	- "${}" are used to include the JS expressions
*/

const interestRate = .5;
const principal = 1000;
console.log(`The interest on your savings account is: ${principal * interestRate}`);


// [SECTION] Array Destructuring
/*
	- Allows us to unpack elements in arrays into distinct variables
	- Allows us to name array elements with variables instead of using index numbers
	- Syntax: 
	let/const [variableNameA, variableNameB, variableNameC] = array;
*/

const fullName = ["Daisy", "Dela", "Cruz"];

// Pre-Array Destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

// Expected Output: Hello Daisy Dela Cruz! It's nice to meet you!
console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`)


// Array Destructuring
const [firstName, middleName, lastName] = fullName;

console.log(firstName);
console.log(middleName);
console.log(lastName);
console.log(fullName);

console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you!`)

// [SECTION] Object Destructuring

let person = {
	givenName: "Jane",
	midName: "Dela",
	familyName: "Cruz"
}

console.log(person.givenName);
console.log(person.midName);
console.log(person.familyName);

// Expected Output: Hello Jane Dela Cruz! it's good to see you!
console.log(`Hello ${person.givenName} ${person.midName} ${person.familyName}! It's good to see you!`);

// Object Destructuring
let { givenName, midName, familyName } = person;

console.log(givenName);
console.log(midName);
console.log(familyName);

console.log(`Hello ${givenName} ${midName} ${familyName}! It's good to see you!`);


// [SECTION] Arrow Functions
/*
	- Compact alternative to traditional functions
	- This will only work with "function expression"
	- Syntax:
		let/const variableName = () => {
			//code block/statement
		}

		let/const variableName = (parameter) => {
			//code block/statement
		}


*/
// function declaration
function greeting(){
	console.log("Hello World!");
}
greeting();


// function expression
const greet = function(){
	console.log("Hi, World!");
}
greet();

// Arrow function

const greet1 = () => {
	console.log("Hello Again!");
}
greet1();

const printFullName = (firstName, middleInitial, lastName) => {
	console.log(`${firstName} ${middleInitial} ${lastName}`);
}
printFullName("John", "D", "Smith");

// Arrow functions with loops
const students = ["John", "Jane", "Judy"];

// Pre-arrow function
students.forEach(function(student) {
	console.log(`${student} is a student.`);
})

// Arrow function
students.forEach((student) => {
	console.log(`${student} is a student.`);
})

// An arrow function has an implicit return value when the curly braces are omitted
const add = (x, y) => x + y;

let total = add(1, 2);
console.log(total);

// [SECTION] Default Function Argument Value
// Provides a default argument value if none is provided when the function is invoked
const greet2 = (name = "User") => {
	return `Good morning, ${name}!`;
}
console.log(greet2());
console.log(greet2("Zoie"));


// [SECTION] Class-Based Object Blueprints

/*
	- Allows creation/instantiation of objects using classes as blueprints
	- The "constructor" is a special method of a class for creating/initializing an object for that class
	- Syntax:
	class className {
		constructor(objectPropertyA, objectPropertyB) {
			this.objectPropertyA = objectPropertyA;
			this.objectPropertyB = objectPropertyB;
		}
	}
*/

class Car {
	constructor(brand, name, year) {
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

// No arguments provided will create an object without any values assigned to it's properties
const myCar = new Car();
console.log(myCar);

myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;
console.log(myCar);

// Instantiating a new object with initialized values
const myNewCar = new Car("Toyota", "Fortuner", 2020);
console.log(myNewCar);
