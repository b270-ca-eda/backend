// console.log("Hello World!");

// Arithmetic Operators
	
	let x = 1397;
	let y = 7831;
	let sum = x + y;
	console.log("Result of additional operator: :" + sum);

	let difference = x - y;
	console.log("Result of subtraction operator: " + difference);

	let product = x * y;
	console.log("Result of multiplication operator: " + product);

	let quotient = y / x;
	console.log("Result of division operator: " + quotient);

	let remainder = y % x;
	console.log("Result of modulo operator: " + remainder);

// Assignment Operators
	
	// Basic Assignment Operator (=)
	let assignmentNumber = 8;

	// Addition Assignment Operator (+=)
	// assignmentNumber = assignmentNumber + 2;
	// console.log("Result of addition assignment operator: " + assignmentNumber);

	// Shorthand for assignNumner = assignNumber + 2;
	assignmentNumber += 2;
	console.log("Result of addition assignment operator: " + assignmentNumber);	

	assignmentNumber -= 2;
	console.log("Result of subtraction assignment operator: " + assignmentNumber);

	assignmentNumber *= 2;
	console.log("Result of multiplication assignment operator: " + assignmentNumber);	

	assignmentNumber /= 2;
	console.log("Result of division assignment operator: " + assignmentNumber);	


	// Multiple Operators
	let number = 1 +2 -3 *4 /5;
	console.log("Result of mdas operator: " + number);

	let pemdas = 1 + ( 2 - 3 ) * ( 4 / 5);
	console.log("Result of pemdas operator: " + pemdas);

//  Type Coercion

	let numA = "10";
	let numB = 12;

	let coercion = numA + numB;
	console.log(coercion);
	console.log(typeof coercion);

	let	numC = 16;
	let numD = 14;
	let noCoercion = numC + numD;
	console.log(noCoercion);
	console.log(typeof noCoercion);

	// The result is a number
	// The "true" is also associated with the value of 1
	let numE = true + 1;
	console.log(numE);

	// The "fale" is also associated with the value of 0
	let numF = false + 1;
	console.log(numF);

// Comparison Operators

	let juan = "juan";

	// Equality Operator (==) - is x equal to y?
	/*
		- Checks whether the operators are equal/have the same content
	*/

	console.log(1==1); //true
	console.log(1==2); // false
	console.log(1=="1"); //true
	console.log(0==false);  //true
	console.log("juan"=="juan");  //true
	console.log("juan"==juan); //true

	// Inequality Operator - is X not equal to y?
	console.log("Inequality Operator")
	console.log(1!=1); //false
	console.log(1!=2); //true
	console.log(1!="1"); //false
	console.log(0!=false); //false
	console.log("juan"!="juan"); //false
	console.log("juan"!=juan); //false

	// Strict Equality Operator
	/*
		-Checks whether the operands are equal/have the same content
		- Also compares the data types of the 2 values
		- Returns a boolean value
	*/
	console.log("Strict Equality Operator")
	console.log(1===1); //true
	console.log(1===2); //false
	console.log(1==="1"); //false
	console.log(0===false); //false
	console.log("juan"==="juan"); //true
	console.log("juan"===juan); //true

	console.log("Strict Inequality Operator")
	console.log(1!==1); //false
	console.log(1!==2); //true
	console.log(1!=="1"); //true
	console.log(0!==false); //true
	console.log("juan"!=="juan"); //false
	console.log("juan"!==juan); //false

// Relational Operators

	let a = 50;
	let b = 65;

	console.log("Relational Operators");

	// Greater than Operator (>)
	let isGreaterThan = a > b;
	console.log(isGreaterThan); //false

	// Less than Operator (<)
	let isLessThan = a<b;
	console.log(isLessThan); //true

	// GTE (>=)
	let isGtOrEqual = a>=b;
	console.log(isGtOrEqual); //false

	// LTE (<=)
	let isLtOrEqual = a<=b;
	console.log(isLtOrEqual); //true


// Logical Operators	

	let isLegalAge = true;
	let isRegistered = false;

	// Logical AND operator (&&)
	// Returns true if all operands are true
	let allRequirementsMet = isLegalAge && isRegistered;
	console.log("Result of operator: " + allRequirementsMet);

	// Logical OR Operator (||)
	// Returns true if one of the operands is true
	let someRequirementsMet = isLegalAge || isRegistered;
	console.log("Result of logical OR operator: " + someRequirementsMet);

	// Logical NOT Operator (!)
	// Returns the opposite value
	let someRequirementsNotMet = !isRegistered;
	console.log("Result of logical NOT operator: " + someRequirementsNotMet);

// Increment and Decrement
	// Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to 
	let z = 1;

	//Pre-increment

	// The value of "z" is added by values of 1 before returning the value and storing it in the variable
	let increment = ++z;
	console.log("Result of pre-increment: " + increment);
	console.log("Result of pre-increment: " + z);

	// Post-increment
	// The value of "z" is returned first and stored in the variable "increment" the nthe value of "z" is increased by 1

	increment = z++;
	// The value of "z" is at 2 before it was incremented
	console.log("Result of post-increment: " + increment);

	// The value of "z" was increased again reassigning the value of 3
	console.log("Result of post-increment: " + z);