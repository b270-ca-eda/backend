const express = require("express");

// This creates an express application and sotres this in a constant called app
// This is our server
const app = express();

const port = 3000;

// Middlewares
// Middleware is a request handler that has access to the application's request-response cycle
app.use(express.json());

// Allows our app to read data from forms
// By default, information received from the url can only be received as a string or an array
// By applying the option of "extended:true" this allows us to receive information in other data types such as an object which we will use throughout our application
app.use(express.urlencoded({extended:true}));


// [SECTION] Routes
// Express has methods corresponding to each HTTP method

// This route expects to receive the GET request at the base URI "/"
// This route will return a simple message back to the client
app.get("/", (req, res) => {

	// res.send uses the Express JS module's method to send a response back to the client
	res.send("Hello World!")
})

// This route expects to receive a POST request at the URI "/hello"
app.post("/hello", (req, res) => {

	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);
})

// mock database
let users = [];

// This route expects to receive a POST request at the URI "/signup"
// This will create a user object in the "users" variable that mirrors a real world registration process
app.post("/signup", (req, res) => {
	console.log(req.body)

	// If contents of the "request body" with the property "username" and "password" is not empty
	if(req.body.username !== "" && req.body.password !== ""){
		users.push(req.body);

		// This will store the user object sent via Postman to the users array created above
		res.send(`User ${req.body.username} is successfully registered!`)

	// If the username and password are not complete an error message will be sent back to the client/Postman
	} else {
		res.send(`Please input BOTH username and password.`)
	}
})

// This route expects to receive a PUT requests at the URI "/change-password"
app.put("/change-password", (req, res) => {
	let message;

	// Creates a for loop that will lopp through the elements of the "users" array
	for (let i = 0; i < users.length; i++){

		// Changes the password of the user found by the loop into the password provided in the client
		if (req.body.username == users[i].username){
			users[i].password = req.body.password;

			message = `User ${req.body.username}'s password has been updated.`;
			break

		// If no user was found
		} else {

			message = `User does not exist.`;
		}		
	}
	res.send(message);
})


// ACTIVITY


app.get("/home", (req, res) => {

	res.send("Welcome to the home page")
})

app.get("/users", (req, res) => {  
response = {  
       username: "johndoe",  
       password: "johndoe1234" 
   };  
   console.log(response);  
   res.end(JSON.stringify(response));  
}) 

app.delete("/delete-user", (req, res) => {  
	res.send("User johndoe has been deleted.")
   })

  


app.listen(port, () => console.log(`Server is running at port ${port}`));
